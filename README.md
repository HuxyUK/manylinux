# manylinux

Custom docker images for building Python compatible binaries

## usage

### docker image creation
```sh
#login
docker login registry.gitlab.com

#x86_64
docker build --rm -t registry.gitlab.com/huxyuk/manylinux/x64-asge x64.asge/
docker push registry.gitlab.com/huxyuk/manylinux:x64-asge

#i686 
docker build -t registry.gitlab.com/huxyuk/manylinux/i686-asge i686.asge/ 
docker push registry.gitlab.com/huxyuk/manylinux:i686-asge
```

### testing buildwheel
```shell
#clone
git clone https://github.com/HuxyUK/pyasge --recursive
cd pyasge

#build env
CIBW_MANYLINUX_X86_64_IMAGE="registry.gitlab.com/huxyuk/manylinux/x64-asge"
export CIBW_MANYLINUX_I686_IMAGE="registry.gitlab.com/huxyuk/manylinux/i686-asge"
export CIBW_BUILD=cp3*-*

#create wheels
python3 -m cibuildwheel --platform linux  --output-dir wheelhouse
```
